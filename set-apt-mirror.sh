#!/bin/sh

UBUNTU_APT_REPO=${UBUNTU_APT_REPO:-http://mirrors.tuna.tsinghua.edu.cn/ubuntu}
DEBIAN_APT_REPO=${DEBIAN_APT_REPO:-http://mirrors.tuna.tsinghua.edu.cn/debian}
DEBIAN_SECURITY_APT_REPO=${DEBIAN_SECURITY_APT_REPO:-http://mirrors.tuna.tsinghua.edu.cn/debian-security}

lsb_dist=""
if [ -r /etc/os-release ]; then
  lsb_dist="$(. /etc/os-release && echo "$ID" | tr '[:upper:]' '[:lower:]')"
fi

case "$lsb_dist" in
  ubuntu)
    sed -i -r "s#^(deb|deb-src)[ ]+\S+[ ]+(.*)#\1 ${UBUNTU_APT_REPO} \2 #g" /etc/apt/sources.list
  ;;
  debian)
    sed -i -r "/debian-security/! s#^(deb|deb-src)[ ]+\S+[ ]+(.*)#\1 ${DEBIAN_APT_REPO} \2 #g" /etc/apt/sources.list
    sed -i -r "/debian-security/ s#^(deb|deb-src)[ ]+\S+[ ]+(.*)#\1 ${DEBIAN_SECURITY_APT_REPO} \2 #g" /etc/apt/sources.list
  ;;
esac
