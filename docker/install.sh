#!/bin/sh
set -e
# Docker CE for Debian/Ubuntu installation script
# See https://docs.docker.com/engine/install/ for the installation steps.

DOCKER_DOWNLOAD_URL="${DOCKER_DOWNLOAD_URL:-https://mirrors.aliyun.com/docker-ce}"
DRY_RUN=${DRY_RUN:-""}

command_exists() {
  command -v "$@" > /dev/null 2>&1
}

install_docker() {

  if command_exists docker; then
    echo
    echo "WARN: Docker command exists. Skipping installation of Docker."
    exit 0
  fi

  case "$lsb_dist" in
    ubuntu|debian)
      #$sh_c 'apt-get update -qq >/dev/null'
      $sh_c "curl -fsSL \"$DOCKER_DOWNLOAD_URL/linux/$lsb_dist/gpg\" | gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/docker-archive-keyring.gpg"
      apt_repo="deb [arch=$(dpkg --print-architecture)] $DOCKER_DOWNLOAD_URL/linux/$lsb_dist $dist_version stable"
      $sh_c "echo \"$apt_repo\" > /etc/apt/sources.list.d/docker.list"
      $sh_c 'apt-get update -qq >/dev/null'
      pkgs="docker-ce-cli docker-ce"
      $sh_c "DEBIAN_FRONTEND=noninteractive apt-get install -y -qq --no-install-recommends $pkgs >/dev/null"
    ;;
    *)
      echo
      echo "WARN: Unsupported distribution '$lsb_dist'. Skipping installation of Docker."
    ;;
  esac

}

get_variables() {

  user="$(id -un 2>/dev/null || true)"
  sh_c='sh -c'
  if [ "$user" != 'root' ]; then
    if command_exists sudo; then
      sh_c='sudo -H -E sh -c'
    elif command_exists su; then
      sh_c='su -c'
    else
      cat >&2 <<-'EOF'
			Error: this installer needs the ability to run commands as root.
			We are unable to find either "sudo" or "su" available to make this happen.
			EOF
      exit 1
    fi
  fi
  if [ ! -z "$DRY_RUN" ]; then
    sh_c="echo"
  fi

  lsb_dist=""
  dist_version=""
  if [ -r /etc/os-release ]; then
    lsb_dist="$(. /etc/os-release && echo "$ID" | tr '[:upper:]' '[:lower:]')"
    dist_version="$(. /etc/os-release && echo "${VERSION_CODENAME}" | tr '[:upper:]' '[:lower:]' )"
  fi

}

get_variables

install_docker

echo All Done.