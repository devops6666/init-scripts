#!/bin/sh
set -e

GET_PIP_URL=${GET_PIP_URL:-https://bootstrap.pypa.io/get-pip.py}
PIP_MIRROR=${PIP_MIRROR:-https://pypi.tuna.tsinghua.edu.cn/simple}
ANSIBLE_VERSION=${ANSIBLE_VERSION:-~=2.9.0}
DRY_RUN=${DRY_RUN:-""}

command_exists() {
  command -v "$@" > /dev/null 2>&1
}

get_variables() {

  user="$(id -un 2>/dev/null || true)"
  sh_c='sh -c'
  if [ "$user" != 'root' ]; then
    if command_exists sudo; then
      sh_c='sudo -H -E sh -c'
    elif command_exists su; then
      sh_c='su -c'
    else
      cat >&2 <<-'EOF'
			Error: this installer needs the ability to run commands as root.
			We are unable to find either "sudo" or "su" available to make this happen.
			EOF
      exit 1
    fi
  fi
  if [ ! -z "$DRY_RUN" ]; then
    sh_c="echo"
  fi

  lsb_dist=""
  dist_version=""
  if [ -r /etc/os-release ]; then
    lsb_dist="$(. /etc/os-release && echo "$ID" | tr '[:upper:]' '[:lower:]')"
    dist_version="$(. /etc/os-release && echo "${VERSION_CODENAME}" | tr '[:upper:]' '[:lower:]' )"
  fi
}

apt_install () {
  $sh_c 'apt-get update -qq >/dev/null'
  $sh_c "DEBIAN_FRONTEND=noninteractive apt-get install -y -qq --no-install-recommends $*"
}

get_variables

if command_exists ansible; then
  echo 'Ansible already installed.'
  echo "$(ansible --version)"
  exit 0
fi

case "$lsb_dist" in
  ubuntu|debian)
    command_exists curl || apt_install curl ca-certificates
    command_exists python3 || apt_install python3
  ;;
esac

for cmd in python3 curl; do
  if ! command_exists $cmd; then
    echo
    echo "ERROR: Can not find $cmd."
    exit 1
  fi
done

if ! command_exists pip3 ; then
  case "$lsb_dist" in
    ubuntu|debian)
      python3 -c 'import distutils.cmd' >/dev/null 2>&1 || apt_install python3-distutils
    ;;
    *)
      echo
      echo 'ERROR: python3-distutils package missing.'
      exit 1
    ;;
  esac
  $sh_c "curl -fsSL ${GET_PIP_URL} | python3 - -i ${PIP_MIRROR} pip wheel setuptools"
fi

$sh_c "pip3 install -i ${PIP_MIRROR} ansible${ANSIBLE_VERSION}"
